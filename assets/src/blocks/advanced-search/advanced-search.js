require('./advanced-search.scss');
const $ = require('jquery');

$('.advanced-search__btn').on('click', function(e) {
    if (!$(this).siblings('.advanced-search__popup').hasClass('advanced-search__popup_active')) {
        e.stopPropagation();
        $(this)
            .siblings('.advanced-search__popup, .advanced-search__triangle')
            .trigger('popup:open');
    }
});

$('.advanced-search__btn-close').on('click', function() {
    $(this).parents('.advanced-search__popup').trigger('popup:close')
        .siblings('.advanced-search__triangle').trigger('popup:close');
});

$('.advanced-search__popup').on('popup:open', function() {
    $(this).addClass('advanced-search__popup_active');
}).on('popup:close', function() {
    $(this).removeClass('advanced-search__popup_active');
}).on('click', function(e) {
    e.stopPropagation();
});

$('.advanced-search__triangle').on('popup:open', function() {
    $(this).addClass('advanced-search__popup_active');
}).on('popup:close', function() {
    $(this).removeClass('advanced-search__popup_active');
});