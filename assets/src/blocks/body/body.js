'use strict';

require('./body.scss');
const $ = require('jquery');

let body = $('.body');

body.on('modal:open', function() {
    $(this).addClass('body_overflow-locked');
});

body.on('modal:close', function() {
    $(this).removeClass('body_overflow-locked');
});

body.on('popup:open', function() {
    $(this).addClass('body_popup-opened');
}).on('popup:close', function() {
    $(this).removeClass('body_popup-opened');
});

body.on('click', function() {
    if ($(this).hasClass('body_popup-opened')) {
        $('.advanced-search__popup, .advanced-search__triangle').trigger('popup:close');
    }
});
