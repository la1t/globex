'use strict';

require('./nav-main.scss');
const $ = require('jquery');

$('.nav-main__burger').on('click', function() {
    $(this).siblings('.nav-main__menu').toggleClass('nav-main__menu_active');
});

$('.nav-wide__burger').on('click', function() {
    $(this).siblings('.nav-wide__menu').toggleClass('nav-wide__menu_active');
})