'use strict';

require('./burger.scss');
const $ = require('jquery');

$('.burger').on('click', function() {
    $(this).toggleClass('burger_active');
});