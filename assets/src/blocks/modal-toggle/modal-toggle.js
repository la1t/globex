'use strict';

const $ = require('jquery');

$('.modal-toggle').on('click', function() {
    let targetId = '#' + $(this).attr('data-modal-target');
    $(targetId).trigger('modal:open');
});