'use strict';

require('./parallax.scss');
const Parallax = require('parallax-js');

let items = document.getElementsByClassName('parallax');

for (let i=0; i < items.length; i++) {
    let item = items[i];
    new Parallax(item);
}
