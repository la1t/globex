'use strict';

require('./select.scss');

const $ = require('jquery');
const jQueryStyler = require('jquery-form-styler/dist/jquery.formstyler');
const WebFont = require('webfontloader');
const malihuPlugin = require('malihu-custom-scrollbar-plugin');
malihuPlugin($);
require('malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css');



jQueryStyler('.select').styler({
    onFormStyled: function() {
        setTimeout(function() {
            $('.jq-selectbox__dropdown ul').mCustomScrollbar({
                axis: 'y',
                scrollInertia: 250,
                theme: 'select',
            });
        }, 500);
    },
    onSelectOpened: function() {
        $(this).find('.jq-selectbox__select-text').addClass('jq-selectbox__select-text_active');
    },
    onSelectClosed: function() {
        $(this).find('.jq-selectbox__select-text').removeClass('jq-selectbox__select-text_active');
    }
});

let webFontConfig = {
    custom: {
        families: ['Istok Web']
    },
    active: function() {
        $('.select').trigger('refresh');
    }
};

WebFont.load(webFontConfig);
