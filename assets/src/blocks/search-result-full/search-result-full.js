'use strict';

require('./search-result-full.scss');
const $ = require('jquery');
const malihuPlugin = require('malihu-custom-scrollbar-plugin');
malihuPlugin($);
require('malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css');


$('.search-result-full__text').mCustomScrollbar({
    axis: 'y',
    scrollInertia: 250,
    theme: 'modal',
});