'use strict';

require('./modal.scss');
const $ = require('jquery');

let items = $('.modal');

items.on('modal:open', function() {
    $(this).addClass('modal_active');
});

items.on('modal:close', function() {
    $(this).removeClass('modal_active');
});

items.on('click', function(event) {
    if (this === event.target) {
        $(this).trigger('modal:close');
    }
});

$('.modal__btn-close').on('click', function() {
    $(this).parents('.modal').trigger('modal:close');
});