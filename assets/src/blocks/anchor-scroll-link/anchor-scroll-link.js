const $ = require('jquery');

$(window).bind('load', function(e) {
    e.preventDefault();
    if (location.hash.length) {
        let doc = $('html, body');
        doc.animate({
            scrollTop: '0px'
        }, {
            duration: 0
        });

        let name = location.hash.substr(1);
        let el = $('#' + name);

        console.log(el);

        doc.animate({
        scrollTop: el.offset().top + 'px'
        }, {
            duration: 1000,
            easing: 'swing'
        });

        window.scrolled = true;
    }
});

$('.anchor-scroll-link').on('click', function() {
    $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top + 'px'
    }, {
        duration: 500,
        easing: 'swing'
    });
    return false;
});